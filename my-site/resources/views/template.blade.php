<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<style>

  /* Style the body */
body {
    font-family: Arial;
    margin: 0;
  }

header {
    overflow: hidden;
    background-color: #f1f1f1;
    padding: 20px 10px;
  }

header .logo {
    float: left;
  }


h1 {
    margin-left: auto;
    margin-right: auto;
    width: 25%
  }
  .header-right {
    float: right;
  }
  /* Page Content */
  .content {padding:20px;}

  </style>
</head>
<body>
    <header>
        <h1>La Nuit De L'informatique</h1>
        <div class="header-right">
        <a href="{{ url('/nuitInfo/part') }}"><button>Partenaire</button></a>
        <a href="{{ url('/nuitInfo/partInscription') }}"><button>Inscription Partenaire</button></a>
        <a href="{{ url('/nuitInfo/orga') }}"><button>Organisateur</button></a>
        <a href="{{ url('/nuitInfo/orgaInscription') }}"><button>Inscription Orga</button></a>
        </div>
    </header>
    @yield('contenu')


</body>
