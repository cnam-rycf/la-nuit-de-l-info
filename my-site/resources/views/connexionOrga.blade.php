@extends('template')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Connexion Organisateur</title>

@section('contenu')
<form action="" method="post">
    <div>
        <label for="libHub">Identifiant Orga</label>
        <input type="text" id="libHub" name="hub_Lib">
    </div>
    <div>
        <label for="locaHub">Mot de passe</label>
        <input type="text" id="locaHub" name="hub_local">
    </div>
    <div class="button">
        <button type="submit">Se connecter</button>
    </div>
</form>
@endsection
