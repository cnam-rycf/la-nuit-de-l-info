@extends('templatePart')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Participants Défis </title>

@section('contenu')
<!--Lire la bdd des défis et afficher la liste avec les balises li -->
<ul id="Choix Défis">
  <li><a onclick="participationDef(\'Defis\')'">Defis All group display</a><li>
</ul>
<div id="container">

</div>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="{{ asset('js/component/readDefis.js')}}"></script>
@endsection
