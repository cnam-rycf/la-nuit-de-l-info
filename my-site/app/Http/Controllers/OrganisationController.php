<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class OrganisationController extends Controller
{

  public function inscripOrga(){

    	return view('inscripOrga');
  }

  public function ValidationOrga(Request $request){
      $libelle = $request->input('hub_Lib');
      $local = $request->input('hub_local');
      $mail = $request->input('hub_mail');
    	return view('inscripOrga',compact('libelle','local','mail'));
  }


}
