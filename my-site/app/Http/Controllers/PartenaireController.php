<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PartenaireController extends Controller
{

  public function inscrip(){

    	return view('inscripPartenaire');
  }

  public function ValidationPart(Request $request){
      $name = $request->input('part_name');
      $mail = $request->input('part_mail');
      $mdp = $request->input('part_mdp');
    	return view('inscripPartenaire',compact('name','mail','mdp'));
  }


}
