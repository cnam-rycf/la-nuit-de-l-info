<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/nuitInfo', function () {
    return view('home');
});

/////////// partenaire///////////////////////////////
///////////////////////////////////////////////////
Route::get('/nuitInfo/part', function () {
    return view('connexionPart');
});

Route::get('/nuitInfo/partDefi','DefiController@DisplayDefi');
Route::get('/nuitInfo/partInscription','partenaireController@inscrip');

Route::post('/nuitInfo/ValidInscriptionPartenaire','partenaireController@ValidationPart');

/////////// organisation///////////////////////////////
///////////////////////////////////////////////////
Route::get('/nuitInfo/orga', function () {
    return view('connexionOrga');
});

Route::get('/nuitInfo/orgaInscription','OrganisationController@inscripOrga');

Route::post('/nuitInfo/ValidInscriptionOrga','OrganisationController@ValidationOrga');


Route::get('/nuitInfo/orgaDef', function () {
    return view('orgaDef');
});

Route::get('/nuitInfo/ajoutDefi', function () {
    return view('ajoutDefi');
});
//////////////////////// Defi///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
Route::get('/nuitInfo/affichageDefi','DefiController@DisplayDefi');
