# Installation

1. git clone [lien]
2. docker-compose up -d --build
3. docker run --rm -v $pwd/my-site:/app composer update
4. Enjoy it !
5. to stop (cf. part "To stop")
## To start

1. docker-compose up

## To stop

1. docker-compose stop

# Référence / Aide

