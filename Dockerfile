FROM php:7.2.2-fpm

RUN apt-get update && apt-get install -y mysql-client --no-install-recommends \
 && docker-php-ext-install pdo_mysql && \
     apt-get install -y \
         libzip-dev libnss3 \
         chromium \
         xvfb gtk2-engines-pixbuf \
         imagemagick x11-apps \
         && docker-php-ext-install zip
